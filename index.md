---
title: Home
nav_order: 1
description: "Courseware as Code is an easy to use website template for education for all skill levels."
permalink: /
---

# Courseware as Code

Welcome to the Courseware as Code template!
This is a template for easily building static websites in Gitlab with big a focus on ease of use for educators, no matter their technical knowledge.

This template started out as a fork of [just-the-docs](https://pmarsceill.github.io/just-the-docs/) and supports the same features.

## Follow along our tutorial

If have already followed our [previous configuration steps](https://gitlab.com/courseware-as-code/courseware-template#courseware-site-template) then you are in the right place!

Follow along our step-by-step tutorial for adding content to your site. Start by {% link course/adding_a_page.md %}