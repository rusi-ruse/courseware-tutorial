---
title: Links to examples
nav_order: 3
---

# Links to examples

If you want some inspiration and references to make your course-creation process easier, check out some of our example courses created with **Courseware as Code**!
All of these are *Open Source*, so you can check out their source code and the resulting course!

- This very same tutorial!
    - [Gitlab repository](https://gitlab.com/courseware-as-code/courseware-template)
    - [Course](https://courseware-as-code.gitlab.io/courseware-template/)

- Git Example used in the tutorial
    - [Gitlab repository](https://gitlab.com/alejandro-rusi/courseware-git-example)
    - [Course](https://alejandro-rusi.gitlab.io/courseware-git-example/)